package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
)

type Data struct {
	Message  string
	Hostname string
}

type Messages map[string]int

func main() {
	hostname, _ := os.Hostname()
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		data := Data{
			Message:  "Hello World",
			Hostname: hostname,
		}
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusCreated)
		json.NewEncoder(w).Encode(data)
	})
	http.HandleFunc("/ping", func(w http.ResponseWriter, r *http.Request) {
		resp := "pong from " + hostname
		fmt.Fprintf(w, resp)
	})
	fmt.Printf("Server running (port=8080), route: http://localhost:8080/\n")
	if err := http.ListenAndServe(":8080", nil); err != nil {
		log.Fatal(err)
	}
}
