FROM golang:1.21-alpine3.17 AS builder

WORKDIR /app

COPY go.* ./

RUN go mod download

COPY *.go ./

RUN go build -o /hello_go_http

FROM alpine:3.17

WORKDIR /app

COPY --from=builder /hello_go_http /hello_go_http

EXPOSE 80

ENTRYPOINT ["/hello_go_http"]
